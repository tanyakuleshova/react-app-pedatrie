module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  important: true,
  theme: {

      extend: {
        fontSize: {

        },
        colors: {
          card_bg: "#F8F8F8",
          dialog_bg: "#ffffff",
          input_bg: "#D1D4DC",
          classic_btn: "#279AF1",
          add_btn: "#F06543",
          delete_bg: "#BB415E",
          icon_bg: "#313D5A",
          bar_bg: "#313D5A",
          dark_font: "#64798A",
          backdrop: "rgba(0, 0, 0, 0.5)",
          spinner_bg: "rgba(255,255,255,0.7)",
        },
        width: {
          width_2xl: "1400px",
          width_xl: "1200px",
          width_lg: "900px",
          width_md: "600px",
          width_m: "800px",
          width_sm: "500px",
          width_smx: "400px",
          width_xs: "300px",
          width_ex: "250px",
          width_exs: "120px"
        },
        maxWidth: {
          width_extra: '2000px',
          width_sm: '200px',
        },
        minHeight: {
          height_xs: '100px',
          height_lg: '400px',
        },
        minWidth: {
          width_xs: '100px'
        },
        height: {
          height_header: "100px",
          height_sm: "400px",
          height_xs: "300px"
        },
        fontFamily: {
          classic: ['Classic', 'sans-serif'],
        },

    },
    // #F8F8F8
  },
  plugins: [],
}
