import React from "react";

const EmptyDocumentWidget = () => {

    return (
        <>

                <div className="document-item my-2 bg-card_bg rounded-md p-4 text-sm">
                    <div className="flex items-center justify-between">
                        <div className="flex-center">

                            <div>
                                <div className="text-sm text-red-400">PDF File</div>
                                <div className="text-gray-600 font-bold">Future Document</div>
                            </div>
                        </div>

                        <div className="w-2/10 flex-center">

                        </div>
                    </div>
                </div>

        </>
    )
}

export default EmptyDocumentWidget;