import React from 'react';
const LoadingPageSpinner = props =>{

    return (
        <div className={`bg-spinner_bg absolute top-0 left-0 bottom-0 right-0 z-50 ${props.className}`}>
            <img style={{ position: "absolute", top: "44vh", left: "50%"}} src="/icons/loading-arrow.gif" alt=""/>
        </div>
    );
}
export default LoadingPageSpinner;