import React from 'react';
const LoadingSpinner = props =>{

    return (
        <div className="absolute flex items-center justify-center">
            {props.isLoading && (<span className="loader"></span>)}
        </div>
    );
}
export default LoadingSpinner;