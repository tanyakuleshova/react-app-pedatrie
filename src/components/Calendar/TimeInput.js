import {useEffect, useState} from "react";
import "./TimeInput.css";
export const TimeInput = (props) => {
    const [updated, setUpdated] = useState(false);


    const timePicker = (id)=>{
        var input = document.getElementById(id);
        var timePicker = document.createElement('div');
        timePicker.classList.add('time-picker');
        input.value = props.initTime;

        let arr = props.initTime.split(":");

        //open timepicker
        input.onclick= function(){
            timePicker.classList.toggle('open');

            this.setAttribute('disabled','disabled');
            timePicker.innerHTML +=`
      <div class="set-time text-sm font-classic">
         <div class="relative">
            <img class="plusH absolute left-2 top-2 cursor-pointer rotate-180" src="/icons/arrow-t-down.svg" alt="">
            <input class="set classic-input mr-2 hour" type="text" value=`+arr[0]+`>
            <img  class="minusH absolute left-2 top-6 cursor-pointer" src="/icons/arrow-t-down.svg" alt="">
         </div>
         <div class="relative">
            <img  class="plusM absolute left-4 top-2 cursor-pointer rotate-180" src="/icons/arrow-t-down.svg" alt="">
            <input class="set classic-input ml-2 minute" type="text" value=`+arr[1]+`>
            <img  class="minusM absolute left-4 top-6 cursor-pointer" src="/icons/arrow-t-down.svg" alt="">
         </div>
      </div>
      <div class="submitTime text-center text-sm text-blue-400 cursor-pointer mt-2">Save</div>`;
            this.after(timePicker);
            var plusH = timePicker.querySelector('.plusH');
            var minusH = timePicker.querySelector('.minusH');
            var plusM = timePicker.querySelector('.plusM');
            var minusM = timePicker.querySelector('.minusM');

            var h = parseInt(timePicker.querySelector('.hour').value);
            var m = parseInt(timePicker.querySelector('.minute').value);
            //increment hour
            plusH.onclick = function(){
                h = isNaN(h) ? 0 : h;
                if(h===23){
                    h =-1;
                }
                h++;
                timePicker.querySelector('.hour').value = (h<10?'0':0)+h;
            }
            //decrement hour
            minusH.onclick = function(){
                h = isNaN(h) ? 0 : h;
                if(h===0){
                    h =24;
                }
                h--;
                timePicker.querySelector('.hour').value = (h<10?'0':0)+h;
            }
            //increment hour
            plusM.onclick = function(){
                m = isNaN(m) ? 0 : m;
                if(m < 5 && m % 5 !== 0){
                    m=0;
                }
                if(m % 5 !== 0){
                    m = m - (m % 5);
                }
                if(m < 55){
                    m +=5;
                }else if (m === 55){
                    m = 0;
                }else{
                    m = 60 - m;
                }
                timePicker.querySelector('.minute').value = (m<10?'0':0)+m;
            }
            //decrement hour
            minusM.onclick = function(){
                m = isNaN(m) ? 0 : m;
                if(m===0){
                    m = 60;
                }
                if(m < 5 && m % 5 !== 0){
                    m=0;
                }
                if(m % 5 !== 0){
                    m = m - (m % 5);
                }
                m -= 5;
                timePicker.querySelector('.minute').value = (m<10?'0':0)+m;
            }

            //submit timepicker
            var submit = timePicker.querySelector(".submitTime");
            submit.onclick = function(){
                input.value = timePicker.querySelector('.hour').value+':'+timePicker.querySelector('.minute').value;
                props.setInitTime(input.value);
                input.removeAttribute('disabled');
                timePicker.classList.toggle('open');
                timePicker.innerHTML = '';
                setUpdated(prevMode => !prevMode);
            }
            // const input = document.getElementById(props.id);
            // input.closest('.modal').onclick = function(e){
            //     const picker = document.querySelector('.time-picker');
            //     console.log(e.target);
            //     if(picker){
            //         if(!e.target.classList.contains('time-picker') || e.target.id !== props.id){
            //             picker.classList.remove('open');
            //         }
            //
            //     }
            // };
        }
    }
    useEffect(() => {
        timePicker(props.id);

    }, [updated]);


    return (
        <>
            <form className="relative">
                <input className="classic-input bg-white font-classic text-sm p-2" type="text" id={props.id}/>
            </form>
        </>
    );
}
