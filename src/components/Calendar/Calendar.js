import React, {useState} from "react";
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";

const Calendar = (props) => {

    var currentYear =  new Date().getFullYear();
    let years = [];
    let startYear = props.startYear ?? 1980;
    while (startYear <= currentYear) {
        years.push(startYear++);
    }
    const months = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
    ];
    return (
        <DatePicker
            popperModifiers={[
                {
                    name: 'arrow',
                    options: { padding: 24 },
                },
            ]}
            className="classic-input text-gray-600"
            dateFormat="yyy-MM-dd"
            placeholder='YYYY-MM-DD'
            renderCustomHeader={({
                                     date,
                                     changeYear,
                                     changeMonth
                                 }) => (
                <div className="flex-center">
                    <div className="relative flex-center w-1/2">
                        <div className="px-2 w-full">
                            <select className="classic-input border-gray-300 text-center mr-2"
                                    value={new Date(date).getFullYear()}
                                    onChange={({target: {value}}) => changeYear(value)}
                            >
                                {years.map((option) => (
                                    <option key={option} value={option}>
                                        {option}
                                    </option>
                                ))}
                            </select>
                            <img className="w-2 absolute right-3 top-4" src="/icons/arrow-t-down.svg" alt=""/>
                        </div>
                    </div>

                    <div className="relative flex-center w-1/2">
                        <div className="px-2 w-full">
                            <select className="classic-input border-gray-300 text-center"
                                    value={months[new Date(date).getMonth()]}
                                    onChange={({target: {value}}) =>
                                        changeMonth(months.indexOf(value))
                                    }
                            >
                                {months.map((option) => (
                                    <option key={option} value={option}>
                                        {option}
                                    </option>
                                ))}
                            </select>
                            <img className="w-2 absolute right-3 top-4" src="/icons/arrow-t-down.svg" alt=""/>
                        </div>
                    </div>
                </div>
            )}
            selected={ props.startDate }
            onChange={(date) => props.setStartDate(date)}
        />
    );
};
export default Calendar;
