import React from "react";
import Input from "../Input/Input";
import {VALIDATOR_REQUIRE} from "../../utils/validation/validators";
import {t} from "i18next";

const Comment = (props) => {
    const comment = props.comment;
    return (
        <form onSubmit={event => {
            event.preventDefault();

            if(comment){
                props.editCommentHandler();
            }else{
                props.addCommentHandler();
            }

        }}>
            <Input
                styles="classic-input"
                marginStyles="mt-4 w-full mr-1"
                placeholder={t("Comment")}
                element="textarea"
                id="comment"
                type="text"
                label=""
                validators={[]}
                initialValue={comment ? comment.description : ''}
                initialValid={false}
                errorText={""}
                onInput={props.inputHandler}
            />
            {comment && (
                <>
                    <div className="w-full cancel-btn mb-2" onClick={()=>{props.setShowEditDialog(false)}}>{t("Cancel")}</div>
                    <button type="submit" className="w-full classic-btn text-center bg-blue-400 cursor-pointer hover:bg-blue-500">{t("SubmitChanges")}</button>
                </>
            )}
            {!comment && (
                <>
                    <button type="submit" className="classic-btn px-4 mt-3" disabled={!props.formState.isValid}>
                        {t("AddComment")}
                    </button>
                </>
            )}

        </form>
    )
}
export default Comment;