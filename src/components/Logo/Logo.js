const Logo = (props)=>{
    const url = props.type === "horizontal" ? "/images/logo-horizontal.svg" : "/images/logo.svg";
    return (
        <div className={`z-20 ${props.style}`}>
            <img src={url} alt=""/>
        </div>
    );
}
export default Logo;