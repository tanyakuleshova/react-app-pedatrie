import React, {useEffect, useState} from "react";
import {t} from "i18next";
import Modal from "../Dialog/Modal";
import EditButton from "../Button/EditButton";

import {helpers} from "../../utils/helpers/helpers";
const ReminderInfoDialog = (props) => {

    console.log(props.reminderData.reminder);
    const {transformTime, transformDate} = helpers();

    return (
        <>
        <Modal
            show={props.showDeleteDialog}
            className={"dialog bg-dialog_bg w-width_smx"}
            onCancel={()=>{props.setShowDeleteDialog(false)}}
            headerClass={"dialog-header text-lg text-red-500"}
            header={t("DeleteComment")}
            contentClass="p-8"
            footerClass="p-4"
            footer={
                <>
                    <div className="w-full cancel-btn mb-2" onClick={()=>{props.setShowDeleteDialog(false); props.setShow(true)}}>{t("Cancel")}</div>
                    <div className="w-full delete-btn" onClick={()=>{props.deleteReminderHandler(props.reminderData.reminder._id)}}>{t("Delete")}</div>
                </>
            }>
            <div className="w-full text-gray-600 text-lg">
                Do you want to delete the reminder for {props.reminderData.name.firstname +" " +props.reminderData.name.lastname}?
            </div>
        </Modal>
        <Modal
            show={props.show}
            className={"dialog bg-dialog_bg"}
            onCancel={()=>{props.setShow(false)}}
            contentClass="p-4"
            >
            <div className="w-full text-gray-600 text-lg">
                {/*<img onClick={()=>{props.setShow(false)}} style={{top: "-40px"}} className="cursor-pointer absolute right-0" src="/icons/close.svg" alt=""/>*/}
                <div className="font-bold mb-3">Reminder</div>
                {props.reminderData.reminder && <div className="border border-gray-200 rounded-md p-3">
                    <div className="flex-between flex-wrap">

                        <div className="text-sm w-1/4">
                            <div className="text-yellow-500">{props.reminderData.relation.category_id}</div>
                            <div className="flex-start mb-2">
                                <div className="font-bold mr-2 text-gray-700">{props.reminderData.name.firstname +" " +props.reminderData.name.lastname}</div>
                            </div>
                        </div>

                        <div className="w-1/4">
                            <div className="mr-4">
                                <div className="text-sm text-gray-400">Appointment</div>
                                <div className="text-sm font-bold">{transformDate(props.reminderData.reminder.appointment_date)}</div>
                                <div className="text-sm font-bold">{transformTime(props.reminderData.reminder.appointment_date)}</div>
                            </div>
                        </div>


                        <div className="w-1/4">
                           <div className="flex-center mr-4">
                               <img className="w-5 cursor-pointer mr-2" src="/icons/reminder.svg"/>
                               <div>
                                   <div className="text-sm text-gray-400">Reminder</div>
                                   <div className="text-sm font-bold">{transformDate(props.reminderData.reminder.reminder_on)}</div>
                                   <div className="text-sm font-bold">{transformTime(props.reminderData.reminder.reminder_on)}</div>
                               </div>
                           </div>
                        </div>
                        <div className="flex-start w-1/4">

                                <EditButton className="justify-center ml-4" text={t("Edit")} onClick={()=>{props.setEditShow(true)}}/>
                                <button type="text" onClick={()=>{props.setShow(false); props.setShowDeleteDialog(true)}} className="ml-2 text-sm flex items-center justify-center text-add_btn">
                                    <img className="w-4 mr-2" src="/icons/remove-circle.svg" alt=""/>
                                    Delete
                                </button>

                        </div>
                    </div>
                </div>}
            </div>
        </Modal>
        </>
    )
}

export default ReminderInfoDialog;
