import React, {useEffect, useState} from "react";
import {t} from "i18next";
import Modal from "../Dialog/Modal";
// import {SingleDate} from "../Calendar/SingleDate";
import {TimeInput} from "../Calendar/TimeInput";
import Alert from "../Alert/Alert";
import variables from "../../constants/variables";
import {surveyApi} from "../../modules/surveys/api/api";
import Calendar from "../Calendar/Calendar";
const ReminderDialog = (props) => {
    const [appointmentDate, setAppointmentReminderDate] = useState(new Date());
    const [appointmentTime, setAppointmentTime] = useState('10:00');
    const [reminderDate, setReminderDate] = useState(new Date());
    const [reminderTime, setReminderTime] = useState('10:00');
    const [errorMessage, setErrorMessage] = useState("");
    const [method, setMethod] = useState("POST");

    const { getReminder, isLoading,  error } = surveyApi();
    useEffect(() => {
        const fetchReminder = async () => {
            try {
                const responseData = await getReminder(props.subClientId);
                if(responseData.data){
                    let reminderDateTime = responseData.data.reminder_on;
                    setReminderDate(new Date(reminderDateTime));

                    let appointmentDateTime = responseData.data.appointment_date;
                    setAppointmentReminderDate(new Date(appointmentDateTime));

                    reminderDateTime = reminderDateTime.split("T");
                    appointmentDateTime = appointmentDateTime.split("T");


                    setAppointmentTime(appointmentDateTime[1].slice(0, 5));
                    setReminderTime(reminderDateTime[1].slice(0, 5));
                    setMethod("PATCH");
                }

            } catch (err) {
                console.log(err);
            }
        };
        fetchReminder();
    }, [props.subClientId]);

    const formatDate = (objectDate) => {
        let day = objectDate.getDate();

        let month = objectDate.getMonth();

        let year = objectDate.getFullYear();

        return year + "-" + month + "-" + day;
    }

    const submitReminder = async () => {

        if(appointmentDate && reminderDate){
            try {
                const reminder_on = formatDate(new Date(reminderDate)) + " "+reminderTime;
                const appointment_date =  formatDate(new Date(appointmentDate))  + " "+appointmentTime;
                const data = {"subclient_id": props.subClientId, "reminder_on": reminder_on, "appointment_date": appointment_date};

                props.submitReminderHandler(data, method);
            } catch (err) {
                setErrorMessage(err);
                setTimeout(()=>{
                    setErrorMessage("");
                }, 2000);
            }
        }else{
            setErrorMessage("Specify dates.");
            setTimeout(()=>{
                setErrorMessage("");
            }, 2000);
        }

    }
    return (
        <Modal
            show={props.show}
            className={"dialog bg-dialog_bg w-width_mx"}
            onCancel={()=>{props.setShow(false)}}
            headerClass={"dialog-header text-lg text-blue-500"}
            contentClass="px-4"
            footerClass="p-4"
            footer={
                <div className="flex-start mt-12">
                    <div className="w-28 classic_btn mr-2" onClick={submitReminder}>Submit</div>
                    <div className="w-28 cancel-btn ml-2" onClick={()=>{props.setShow(false)}}>Cancel</div>
                </div>
            }>
            <div className="w-full text-gray-600 text-lg">
                <div className="font-bold">Generate a link</div>
                <div className="relative text-sm">
                    <img className="absolute right-3 top-6" src="/icons/link.svg" alt=""/>
                    <input placeholder="https://example.com/647346334fgdhsfjdj" type="text"
                           className="classic-input mt-2 p-3"/>
                </div>
                <div className="font-bold mt-4 mb-2">Set appointment date</div>
                <div className="flex items-start justify-between  text-sm">
                    {/*<SingleDate initDate={appointmentDate} setInitDate={setAppointmentReminderDate} id={"234"}/>*/}
                    <div className="w-1/2 mr-2">
                    <Calendar
                        startDate={appointmentDate}
                        setStartDate={setAppointmentReminderDate}
                        startYear={new Date().getFullYear()}
                    >
                    </Calendar>
                    </div>
                    <div className="w-1/2">
                        <TimeInput id={"picker123"} initTime={appointmentTime} setInitTime={setAppointmentTime} key={"1234"}/>
                    </div>
                </div>
                <div className="font-bold mt-4 mb-2">Add a reminder</div>
                <div className="flex items-start justify-between text-sm">
                    {/*<SingleDate initDate={reminderDate} setInitDate={setReminderDate} id={"837"}/>*/}
                   <div className="w-1/2 mr-2">
                       <Calendar
                           startDate={reminderDate}
                           setStartDate={setReminderDate}
                           startYear={new Date().getFullYear()}
                       >
                       </Calendar>
                   </div>
                    <div className="w-1/2">
                        <TimeInput id={"picker567"} initTime={reminderTime} setInitTime={setReminderTime} key={"758"}/>
                    </div>
                </div>
                <div className="absolute z-50 flex-center w-full">
                    <Alert error={errorMessage}/>
                </div>
            </div>
        </Modal>
    )
}

export default ReminderDialog;
