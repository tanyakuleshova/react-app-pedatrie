const Footer = ()=>{
    return (
        <div className="w-full absolute bottom-0 h-0 md:h-28 xl:h-56 bg-cover bg-repeat-x z-10"
             style={{backgroundImage: `url('/images/footer.svg')`}}></div>
    );
}
export default Footer;