import AddButton from "../Button/AddButton";
import {t} from "i18next";

const PageHeader = ({children}) => {
    return (
        <div className="mb-4">
            <div className="relative">
                <img className="absolute top-4 left-4 w-3" src="/icons/search.svg" alt=""/>
                <input type="text" placeholder="Search" className="pl-10 classic-input"/>
            </div>
            <div className="mt-4 text-sm text-gray-600">
                <div className="mb-2">{t("FilterBy")}</div>
                {children}
            </div>
        </div>
    )
}
export default PageHeader;