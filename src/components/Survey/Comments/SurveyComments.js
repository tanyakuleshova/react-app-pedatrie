import {t} from "i18next";
import React, {useEffect, useState} from "react";
import {useForm} from "../../../hooks/form-hook";
import Modal from "../../Dialog/Modal";
import EditButton from "../../Button/EditButton";
import {helpers} from "../../../utils/helpers/helpers";
import Comment from "../../Comment/Comment";
import {surveyApi} from "../../../modules/surveys/api/api";

const SurveyComments = (props) => {
    const {transformDate, strLimit} = helpers();
    const survey_assign_id = props.assign_id;

    const [comments, setComments] = useState();
    const [isLoading, setIsLoading] = useState(false);
    const [comment, setComment] = useState("");
    const [formState, inputHandler, setFormData] = useForm();
    const [updated, setUpdated] = useState(false);
    const [showDialog, setShowDialog] = useState(false);
    const [showEditDialog, setShowEditDialog] = useState(false);

    const { addComment, getComments, deleteComment, editComment, error, clearError } = surveyApi();
    useEffect(() => {
        const fetchComments = async () => {
            try {
                const responseData = await getComments(survey_assign_id);
                const items = responseData.data.result;
                setComments(items);
            } catch (err) {
                console.log(err);
            }
        };

        fetchComments();
    }, [updated])
    const addCommentHandler = async event => {
        clearError();
        let data = {
            survey_assign_id: survey_assign_id,
            description: formState.inputs.comment.value,
        };

        data = JSON.stringify(data);
        if(formState.inputs.comment.value){
            try {
                setIsLoading(true);
                try {
                    const responseData = await addComment(data);
                    setUpdated(prevMode => !prevMode);
                    setIsLoading(false);
                } catch (err) {
                    setIsLoading(false);
                }
            } catch (err) {
                console.log(err);
            }
        }

    }
    const showDeleteCommentDialogue = (item) => {
        setShowDialog(true);
        setComment(item);
    }

    const showEditCommentDialogue = (item) => {
        setShowEditDialog(true);
        setComment(item);
    }
    const deleteCommentHandler = async event => {

        event.preventDefault();

        clearError();
        setIsLoading(true);
        try {
            setShowDialog(false);
            const responseData = await deleteComment(comment._id);
            setUpdated(prevMode => !prevMode);
            setIsLoading(false);
        } catch (err) {
            setIsLoading(false);
        }
    }

    const editCommentHandler = async event => {
        clearError();
        setIsLoading(true);
        if(formState.inputs.comment.value){
            try {
                setShowEditDialog(false);
                const data = {
                    description: formState.inputs.comment.value,
                    _id: comment._id
                }
                const responseData = await editComment(JSON.stringify(data));
                setUpdated(prevMode => !prevMode);
                setIsLoading(false);
            } catch (err) {
                console.log(err);
                setIsLoading(false);
            }
        }
    }



    return (
        <>
                <div className="text-gray-600">
                    {comment && (<Modal
                        show={showDialog}
                        className={"dialog bg-dialog_bg w-width_smx"}
                        onCancel={()=>{setShowDialog(false)}}
                        headerClass={"dialog-header text-lg text-red-500"}
                        header={t("DeleteComment")}
                        contentClass="p-8"
                        footerClass="p-4"
                        footer={
                            <>
                                <div className="w-full cancel-btn mb-2" onClick={()=>{setShowDialog(false)}}>{t("Cancel")}</div>
                                <div className="w-full delete-btn" onClick={deleteCommentHandler}>{t("Delete")}</div>
                            </>
                        }>
                        <div className="w-full text-gray-600 text-lg">
                            {t("ConfirmDeleteComment")} "{strLimit(comment.description)}"?
                        </div>
                    </Modal>)}
                    {comment && (
                        <Modal
                            show={showEditDialog}
                            className={"dialog bg-dialog_bg w-width_smx"}
                            onCancel={()=>{setShowEditDialog(false)}}
                            headerClass={"dialog-header text-lg text-blue-500"}
                            header={t("EditComment")}
                            contentClass="p-4"
                        >
                            <div className="w-full text-gray-600 text-lg">
                                <Comment
                                    addCommentHandler={addCommentHandler}
                                    comment={comment}
                                    inputHandler={inputHandler}
                                    formState={formState}
                                    setShowEditDialog={setShowEditDialog}
                                    editCommentHandler={editCommentHandler}
                                />
                            </div>
                        </Modal>
                    )}


                    <div className="">
                        <div className="font-bold my-2 text-lg">{t("Comments")}</div>
                        <div className="my-2">
                            {comments && comments.map(item =>
                                <div key={item._id} className="relative rounded-md bg-card_bg my-2 p-4 text-gray-500">
                                    <div className="flex items-center justify-end">
                                        <EditButton className="justify-center ml-4" onClick={()=>{showEditCommentDialogue(item)}} text={t("Edit")}/>
                                        <img onClick={()=>{showDeleteCommentDialogue(item)}}
                                             className="ml-2 h-4 w-4 cursor-pointer"
                                             src="/icons/delete.svg" alt=""/>
                                    </div>
                                    <div className="text-sm"> {item.description}</div>
                                    <div className="text-right text-sm">{transformDate(item.created_date)}</div>
                                </div>
                            )}
                            {!isLoading && <Comment addCommentHandler={addCommentHandler} inputHandler={inputHandler} formState={formState}/>}
                        </div>
                    </div>
                </div>

        </>
    )
}
export default SurveyComments;