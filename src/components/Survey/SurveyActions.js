import {t} from "i18next";

const SurveyActions = (props) => {
    return (
        <>
            <div className="flex items-center justify-between">
                <div className="flex items-center">
                    <div className="relative w-width_ex mr-3">
                        <select className=" classic-input bg-transparent py-3" name="" id="">
                            <option value="">{t("AddedRecently")}</option>
                        </select>
                        <img className="absolute right-2 top-5" src="/icons/arrow-t-down.svg" alt=""/>
                    </div>
                    <div className="relative w-width_ex mr-3">
                        <select className=" classic-input bg-transparent py-3" name="" id="">
                            <option value="">{t("Language")}</option>
                        </select>
                        <img className="absolute right-2 top-5" src="/icons/arrow-t-down.svg" alt=""/>
                    </div>
                </div>
            </div>
        </>
    )
}

export default SurveyActions;