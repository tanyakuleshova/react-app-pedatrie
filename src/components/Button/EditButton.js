import React from 'react';
const EditButton = props =>{

    return (
        <div onClick={props.onClick} className={`flex items-center cursor-pointer text-blue-400 hover:text-blue-500 text-sm font-bold ${props.className}`}>
            <img className="mr-2" src="/icons/edit.svg" alt=""/> {props.text}
        </div>
    );
}
export default EditButton;