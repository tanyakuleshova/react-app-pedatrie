import React, { useRef, useState, useEffect } from 'react';

import './ImageUpload.css';
import AddButton from "../Button/AddButton";
import {t} from "i18next";
import variables from "../../constants/variables";

const ImageUpload = props => {
    const [file, setFile] = useState();
    const [previewUrl, setPreviewUrl] = useState([]);
    const [isValid, setIsValid] = useState(false);

    const filePickerRef = useRef();

    useEffect(() => {
        if (!file) {
            return;
        }
        var arr=[];
        for(let i=0; i<=file.length-1;i++){
            if(i < variables.maxUploadFiles){
                arr.push(file[i]);
            }
        }
        setPreviewUrl(arr);
        const fileReader = new FileReader();
        fileReader.onload = () => {
            console.log(fileReader);
            setPreviewUrl(file);
        };
        // fileReader.readAsDataURL(file);
    }, [file]);

    const pickedHandler = event => {
        let pickedFile;
        let fileIsValid = isValid;
        if (event.target.files) {
            pickedFile = event.target.files;
            setFile(pickedFile);
            setIsValid(true);
            fileIsValid = true;
        } else {
            setIsValid(false);
            fileIsValid = false;
        }
        props.onInput(props.id, pickedFile, fileIsValid);
    };

    const pickImageHandler = () => {
        filePickerRef.current.click();
    };

    return (
        <div className="form-control">
            <input
                id={props.id}
                ref={filePickerRef}
                style={{ display: 'none' }}
                type="file"
                accept=".pdf"
                onChange={pickedHandler}
                multiple
            />
            <div className={`image-upload ${props.center && 'center'}`}>
                <div className="flex flex-wrap">
                    {Array.from(previewUrl).map(item =>
                        <div key={item.name} className="m-1 bg-card_bg rounded-md p-2 text-sm">{item.name}</div>
                    )}
                </div>
                {!previewUrl.length &&
                    <div className="my-1 bg-card_bg rounded-md p-2 text-sm my-2">No documents selected</div>
                }
                <div onClick={pickImageHandler}>
                    <AddButton className={"w-1/2 text-sm"} btn_text={t("UploadDocument")}/>
                </div>
            </div>
            {!isValid && <p>{props.errorText}</p>}
        </div>
    );
};

export default ImageUpload;