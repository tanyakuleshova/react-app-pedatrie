import Logo from "../Logo/Logo";
import variables from "../../constants/variables";
import {Link, useLocation, useNavigate} from "react-router-dom";
import {t} from "i18next";
import {AuthContext} from "../../context/auth-context";
import {useContext} from "react";

const Sidebar = ()=>{
    const routeName = useLocation();
    const auth = useContext(AuthContext);
    const navigate = useNavigate();

    return (
        <div className="w-width_xs p-4">
            <div className="h-height_header">
                <Logo type={"horizontal"} style={"w-2/3"}></Logo>
            </div>
            {variables.navigation.map(item =>
                <Link key={item.title} to={item.route}>
                    <div className={`mb-8 flex items-center justify-start font-bold cursor-pointer ${routeName.pathname === item.route ? '' : 'text-gray-500'}`}>
                        <img className="mr-2" src={`/icons/${item.icon}${routeName.pathname === item.route ? '-active' : ''}.svg`} alt=""/>
                        <div>{t(item.title)}</div>
                    </div>
                </Link>
            )}
            <div onClick={()=>{auth.logout(); navigate("/login");}} className="text-red-500 cursor-pointer">{t("SignOut")}</div>
        </div>
    )
}

export default Sidebar;