import {Link} from "react-router-dom";
import AddButton from "../Button/AddButton";
import {t} from "i18next";
import React from "react";

const ClientsNotFound = () => {
    return (
        <div className="flex-center" style={{height:'calc(100vh - 200px)'}}>
            <div>
                <div className="flex-center">
                    <img src="/icons/user-circle.svg" className="mr-2" alt=""/>
                    <div>
                        <img src="/icons/line-blue.svg" className="my-1" alt=""/>
                        <img src="/icons/line-white.svg" className="my-1" alt=""/>
                        <img src="/icons/line-white.svg" alt=""/>
                    </div>
                </div>
                <div className="my-3 font-bold text-3xl w-width_xs text-center">
                    {t("haveNoClient")}
                </div>
                <div className="my-3 w-width_xs text-center text-md text-gray-400">
                    {t("startAddingClient")}
                </div>
                <Link to="/clients/new" className="flex-center my-3"><AddButton btn_text={t("AddClient")} /></Link>
            </div>
        </div>
    )
}
export default ClientsNotFound;