import {Link} from "react-router-dom";
import React from "react";
import {helpers} from "../../utils/helpers/helpers";
import EditButton from "../Button/EditButton";
import {t} from "i18next";

const ClientCard = (props) => {
    const loadedClient = props.loadedClient;
    const {transformDate} = helpers();
    return (
        <div className="bg-card_bg p-5">
            <div className="flex items-start justify-start">
                <img className="w-16" src="/icons/user-circle.svg" alt=""/>
                <div className="ml-4">
                    <div className="text-red-500 text-lg">{t("Child")}</div>
                    <div className="text-2xl font-bold mt-2">{loadedClient.name.firstname + " " +loadedClient.name.lastname}</div>
                    <div className="items-center flex justify-start">
                        <div className="text-gray-500 text-sm">Added {transformDate(loadedClient.created_date)}</div>
                        <Link to={`/clients/edit/${loadedClient._id}`}>
                            <EditButton className="justify-center ml-4" text={t("EditInfo")}/>
                        </Link>
                    </div>
                    <div className="text-gray-500 text-sm mt-3">{t("DoB")}</div>
                    <div className="text-lg font-bold">{transformDate(loadedClient.info.date_of_birth)}</div>
                    <div className="text-gray-500 text-sm mt-3">{t("Description")}</div>
                    <div className="text-lg font-bold">{loadedClient.info.description}</div>
                </div>
            </div>
        </div>
    )
}
export default ClientCard;