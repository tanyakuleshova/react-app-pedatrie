import React, {useState} from "react";
import {t} from "i18next";

const EmptyClientWidget = () => {

    return (
        <>
                <div className="bg-card_bg rounded-md p-4 my-4 text-sm">
                    <div className="flex items-center justify-between">

                        <div className="w-width_exs">
                            <div className="bg-blue-400 rounded-full p-4 w-fit">
                                <img className="w-4 h-4" src="/icons/user-white.svg" alt=""/>
                            </div>
                            <div className="text-yellow-500 mt-3">{t("Child")}</div>
                            <div className="font-bold mb-2 text-gray-700">Future Patient</div>
                            <div className="text-gray-500 text-sm">Future date</div>
                            <div className="text-gray-500 text-sm"></div>

                        </div>
                        <div className="bg-white p-4 w-4/5 rounded-md">
                            <div className="text-sm font-bold mb-2 text-gray-600">{t("SubClients")}</div>
                            <div className="flex items-center border rounded-md p-4 min-h-height_xs overflow-hidden">

                                    <div className="w-full rounded-md bg-gray-100">
                                        <div className="flex-center p-10 text-gray-400 text-lg font-bold">{t("FutureSubclients")}</div>
                                    </div>

                            </div>
                        </div>
                        <div className="w-1/10">

                                <img className="mr-4" src="/icons/arrow-right.svg" alt=""/>

                        </div>
                    </div>
                </div>

        </>
    )
}

export default EmptyClientWidget;