import React, {useState} from "react";
import {t} from "i18next";
import {helpers} from "../../utils/helpers/helpers";

const ClientCard = (props) => {
    const item = props.item;
    const email = props.item ? item.contact_info.email_address : "";
    const {shorterString} = helpers();
    const [hidden, setHidden] = useState("hidden");
    const copyEmail = ()=>{
        navigator.clipboard.writeText(email);
        setHidden("");
        setTimeout(()=>{
            setHidden("hidden");
        }, 500);
    }
    return (
        <div key={item._id} className="xl:w-1/4 lg:w-1/2 md:w-1/2 w-full ">
            <div className="m-2 p-2 border rounded-md">
                {/*<div className="text-sm text-red-400">{item.relation.category_id}</div>*/}
                <div className="text-sm text-red-400">{item.relation.category_id.label}</div>
                <div className="text-sm font-bold mb-2">{item.name.firstname + " " + item.name.lastname}</div>
                <div className="flex-start text-sm text-gray-500">
                    <div>Email</div>
                    <div className={`text-green-600 ml-2 text-sm ${hidden}`}>Copied!</div>
                </div>
                <div className="flex-start mb-2">
                    <img onClick={copyEmail} className="w-4 mr-2 cursor-pointer"  src="/icons/copy.svg" alt=""/>
                    <div className="text-sm font-bold ">{shorterString(email)}</div>
                </div>
                <div className="text-sm text-gray-500">{t("PhoneNumber")}</div>
                <div className="text-sm font-bold mb-2">{item.contact_info.phone_number}</div>
                <div className="text-sm text-gray-500">{t("Location")}</div>
                <div className="text-sm font-bold mb-2">{item.info.location}</div>
                <div className="text-sm text-gray-500">{t("Surveys")}</div>
                <div className="text-sm text-green-500 font-bold">{t("Complete")}</div>
            </div>
        </div>
    )
}
export default ClientCard;