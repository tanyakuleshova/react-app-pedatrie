import React, {useState} from "react";
import Input from "../Input/Input";
import {VALIDATOR_REQUIRE} from "../../utils/validation/validators";
import {t} from "i18next";
import Calendar from "../Calendar/Calendar";

const ClientForm = props => {
    const client = props.client || "";
    const formState = props.formState || {};
    const dateOfBirth = client.date_of_birth ? new Date(client.date_of_birth) : new Date();

    const [dob, setDob] = useState(dateOfBirth);
    return (
        <React.Fragment>
        {client && (
            <form onSubmit={(e)=>{e.preventDefault();props.clientSubmitHandler(dob)}}
                  className="border rounded-md p-3 border-gray-50 shadow-sm text-sm text-gray-700">
            <div className="flex items-center justify-between">
                <div className="font-bold text-lg text-gray-600">{t("AddClient")}</div>
                <div className="flex items-center justify-center">
                    {client.id && <button onClick={(event)=>{event.preventDefault(); props.showDeleteDialog();}}
                                          type="text" className="flex items-center justify-center mr-4 text-delete_bg">
                        <img className="w-4 mr-2" src="/icons/remove-circle-red.svg" alt=""/>
                        {t("RemoveClient")}
                    </button>}
                    <button type="submit" className="classic-btn px-4" disabled={!formState.isValid}>
                        {client.id ? t("SaveChanges") : t("Submit")}
                    </button>
                </div>
            </div>
            <div className="text-gray-600 font-bold">{t("ChildInfo")}</div>

            <div className="flex items-center justify-between">
                <Input
                    styles="classic-input"
                    marginStyles="mt-4 w-1/2 mr-1"
                    placeholder={t("FirstName")}
                    element="input"
                    id="firstname"
                    type="text"
                    label={t("FirstName")}
                    validators={[VALIDATOR_REQUIRE()]}
                    initialValue={client.firstname || ''}
                    initialValid={!!client.firstname}
                    errorText={t("FieldRequired")}
                    onInput={props.inputHandler}
                />
                <Input
                    styles="classic-input"
                    marginStyles="mt-4 w-1/2 ml-1"
                    placeholder={t("LastName")}
                    element="input"
                    id="lastname"
                    type="text"
                    label={t("LastName")}
                    validators={[VALIDATOR_REQUIRE()]}
                    initialValue={client.lastname || ''}
                    initialValid={!!client.lastname}
                    errorText={t("FieldRequired")}
                    onInput={props.inputHandler}
                />
            </div>

                <div className="mt-4 text-gray-500 text-sm">{t("DoB")}</div>
                <Calendar
                    startDate={dob}
                    setStartDate={setDob}
                >
                </Calendar>
            <Input
                styles="classic-input hidden"
                marginStyles="mt-4 w-full ml-1"
                placeholder="Year - Month - Day"
                element="input"
                id="date_of_birth"
                type="text"
                label=""
                validators={[VALIDATOR_REQUIRE()]}
                initialValue={client.date_of_birth || ''}
                initialValid={true}
                errorText={t("FieldRequired")}
                onInput={props.inputHandler}
            />
            <Input
                styles="classic-input"
                marginStyles="mt-4 w-full ml-1"
                placeholder={t("AddDescription")}
                id="description"
                type="text"
                label={t("Description")}
                initialValue={client.description || ''}
                initialValid={true}
                onInput={props.inputHandler}
                validators={[]}
            />
        </form>
        )}
        </React.Fragment>
    )
}
export default ClientForm;