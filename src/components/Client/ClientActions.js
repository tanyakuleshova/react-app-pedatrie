import AddButton from "../Button/AddButton";
import {t} from "i18next";
import {Link} from "react-router-dom";

const ClientActions = (props) => {
    return (
        <>
            <div className="flex flex-wrap items-center justify-between">
                <div className="flex items-center flex-wrap">

                    <div className="relative w-width_ex mr-3">
                        <select className=" classic-input bg-transparent py-3" name="" id="">
                            <option value="">{t("AddedRecently")}</option>
                        </select>
                        <img className="absolute right-2 top-5" src="/icons/arrow-t-down.svg" alt=""/>
                    </div>
                    <div className="relative w-width_ex mr-3">
                        <select className=" classic-input bg-transparent py-3" name="" id="">
                            <option value="">{t("CompletedSurveys")}</option>
                        </select>
                        <img className="absolute right-2 top-5" src="/icons/arrow-t-down.svg" alt=""/>
                    </div>
                    <div className="relative w-width_ex mr-3">
                        <select className=" classic-input bg-transparent py-3" name="" id="">
                            <option value="">{t("UploadedDocuments")}</option>
                        </select>
                        <img className="absolute right-2 top-5" src="/icons/arrow-t-down.svg" alt=""/>
                    </div>
                </div>
                {props.btn_text && <Link to={props.btn_link}><AddButton btn_text={props.btn_text} /></Link>}
            </div>
        </>
    )
}

export default ClientActions;