import React, {useState} from "react";
import {t} from "i18next";
import {Link} from "react-router-dom";
import {helpers} from "../../utils/helpers/helpers";
import EditButton from "../Button/EditButton";
import ReminderInfoDialog from "../Reminder/ReminderInfoDialog";
import ReminderDialog from "../Reminder/ReminderDialog";
import Alert from "../Alert/Alert";
import {surveyApi} from "../../modules/surveys/api/api";

const ClientWidget = (props) => {
    const {transformDate} = helpers();
    const [showReminderInfoDialog, setShowReminderInfoDialog] = useState(false);
    const [showReminderDialog, setShowReminderDialog] = useState(false);
    const [showDeleteDialog, setShowDeleteDialog] = useState(false);
    const [subClientId, setSubClientId] = useState(false);
    const [reminderData, setReminderData] = useState(false);
    const [message, setMessage] = useState("");
    const [errorMessage, setErrorMessage] = useState("");
    const {  submitReminder, deleteReminder, isLoading,  error } = surveyApi();
    const handleShowReminderInfo = (subClientId, data)=>{
        setSubClientId(subClientId);
        setShowReminderInfoDialog(true);
        setReminderData(data);
    }
    const submitReminderHandler = async (data, method='PATCH') => {
        const responseData = await submitReminder(JSON.stringify(data), method);
        if (responseData.success) {
            setMessage("Success!");
            setShowReminderDialog(false);
            setShowReminderInfoDialog(false);
            props.setUpdated(prevMode => !prevMode);
            setTimeout(() => {
                setMessage("");
            }, 2000);
        }else{
            setErrorMessage(responseData.error);
            setTimeout(()=>{
                setErrorMessage("");
            }, 2000);
        }
    }
    const deleteReminderHandler = async (id) => {
        let data = {"subclient_id": subClientId, "deleted": true}
        const responseData = await deleteReminder(JSON.stringify(data));
        if (responseData.success) {
            setMessage("Success!");
            setShowReminderDialog(false);
            setShowReminderInfoDialog(false);
            setShowDeleteDialog(false);
            props.setUpdated(prevMode => !prevMode);
            setTimeout(() => {
                setMessage("");
            }, 2000);
        }else{
            setErrorMessage(responseData.error);
            setTimeout(()=>{
                setErrorMessage("");
            }, 2000);
        }
    }
    return (
        <>
        {subClientId && <ReminderInfoDialog
            subClientId={subClientId}
            showDeleteDialog={showDeleteDialog}
            setShowDeleteDialog={setShowDeleteDialog}
            reminderData={reminderData}
            setEditShow={setShowReminderDialog}
            show={showReminderInfoDialog}
            deleteReminderHandler={deleteReminderHandler}
            setShow={setShowReminderInfoDialog}/>
        }
        {subClientId && <ReminderDialog
            submitReminderHandler={submitReminderHandler}
            subClientId={subClientId}
            show={showReminderDialog}
            setShow={setShowReminderDialog}/>
        }
            <div className="absolute z-50 flex-center w-full">
                <Alert error={errorMessage || error} message={message}/>
            </div>
            {props.items.map(item =>
                <div key={item._id} className="bg-card_bg rounded-md p-4 my-4 text-sm">
                    <div className="flex items-center justify-between">

                        <div className="w-width_exs">
                            <div className="bg-blue-400 rounded-full p-4 w-fit">
                                <img className="w-4 h-4" src="/icons/user-white.svg" alt=""/>
                            </div>
                            <div className="text-yellow-500 mt-3">{t("Child")}</div>
                            <div className="font-bold mb-2 text-gray-700">{item.name.firstname +" " +item.name.lastname}</div>
                            <div className="text-gray-500 text-sm">{t("AddedOn")}</div>
                            <div className="text-gray-500 text-sm">{transformDate(item.created_date)}</div>
                            <Link to={`/clients/edit/${item._id}`}>
                                <EditButton className="justify-start" text={t("EditInfo")}/>
                            </Link>
                        </div>
                        <div className="bg-white p-4 w-4/5 rounded-md">
                            <div className="text-sm font-bold mb-2 text-gray-600">{t("SubClients")}</div>
                            <div className="flex items-center border rounded-md p-4 min-h-height_xs overflow-hidden">
                                {item.subclients.map(item =>
                                <div key={item._id} className="mr-6 min-w-width_xs">
                                    <div className="text-yellow-500">{item.relation.category_id}</div>
                                   <div className="flex-start mb-2">
                                       <div className="font-bold mr-2 text-gray-700">{item.name.firstname +" " +item.name.lastname}</div>
                                       {item.reminder && <img onClick={()=>handleShowReminderInfo(item._id, item)} className="w-6 cursor-pointer" src="/icons/reminder.svg" alt=""/>}
                                   </div>
                                    <div className="text-gray-500 text-sm">{t("Surveys")}</div>
                                    <div className="text-green-500 font-bold">{t("Complete")}</div>
                                </div>
                                )}
                                {item.subclients.length < 1 && (
                                    <div className="w-full rounded-md bg-gray-100">
                                        <div className="flex-center p-10 text-gray-400 text-lg font-bold">{t("FutureSubclients")}</div>
                                    </div>
                                )}
                            </div>
                        </div>
                        <div className="w-1/10">
                            <Link to={`/clients/details-tab/${item._id}`}>
                                <img className="mr-4" src="/icons/arrow-right.svg" alt=""/>
                            </Link>
                        </div>
                    </div>
                </div>
            )}
        </>
    )
}

export default ClientWidget;