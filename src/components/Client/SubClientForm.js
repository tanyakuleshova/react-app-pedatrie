import React, {useCallback, useMemo, useState} from "react";
import Input from "../Input/Input";
import {VALIDATOR_EMAIL, VALIDATOR_MINLENGTH, VALIDATOR_REQUIRE} from "../../utils/validation/validators";
import {t} from "i18next";
import {useForm} from "../../hooks/form-hook";

const SubClientForm = props => {

    const client = props.subclient ? {
        firstname: props.subclient.name.firstname,
        lastname: props.subclient.name.lastname,
        email: props.subclient.contact_info.email_address,
        phone_number: props.subclient.contact_info.phone_number,
        location: props.subclient.info.location,
        category_id: props.subclient.relation.category_id._id,
        id: props.subclient._id,
        patient_id: props.subclient.relation.patient_id
    } : "";

    const [newCategoryName, setNewCategoryName] = useState("");
    const [isEmpty, setIsEmpty] = useState(!props.subclient);
    const [categoryID, setCategoryID] = useState(client ? client.category_id : "");
    const uniqueCategorySelector = "categoryID_"+ (Math.random() + 1).toString(36).substring(7);

    const categoryName = useMemo(() =>
    {
        if(props.categories && categoryID){
            for(let i=0; i <= props.categories.length-1; i++){

                if(props.categories[i]._id === categoryID){
                    return props.categories[i].label;
                }
            }
        }
        return "";
    }, [categoryID, newCategoryName]);


    const [formStateSub, inputHandlerSub] = useForm();
    const [isHidden, setIsHidden] = useState("hidden");


    const changeCategory = (item) => {
        const categoryInput = document.getElementById("u_"+uniqueCategorySelector);
        setCategoryID(item._id);
        setIsHidden("hidden");
        categoryInput.value = item.label;
        categoryInput.classList.remove('border-red-500');
    };


    const subClientData = useCallback( () => {

        let data = {
            firstname: formStateSub.inputs.firstname.value,
            lastname: formStateSub.inputs.lastname.value,
            email_address: formStateSub.inputs.email_address.value,
            phone_number: formStateSub.inputs.phone_number.value,
            location: formStateSub.inputs.location.value,
            subclient_id: client.id,
            category_id: categoryID,
            patient_id: client.patient_id,
        };

        if(data.category_id === ""){
            data.other_category = newCategoryName;
        }

        return JSON.stringify(data);


    }, [formStateSub, categoryID, newCategoryName]);
    const filterRelation = () =>{

        setCategoryID("");
        setIsHidden("");
        setIsEmpty(false);
        let input, filter, ul, li, a, i, txtValue, exist;
        exist = false;
        input = document.getElementById("u_"+uniqueCategorySelector);
        input.classList.remove('border-red-500');
        filter = input.value.toUpperCase();
        ul = document.getElementById("ul_"+uniqueCategorySelector);
        li = ul.getElementsByTagName("li");

        for (i = 0; i < li.length; i++) {
            txtValue = li[i].innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
                exist = true;
            } else {
                li[i].style.display = "none";
            }
        }
        if(input.value === ""){
            input.classList.add('border-red-500');
            setIsEmpty(true);
        }
        if(!exist){
            setIsHidden("hidden");
        }

    }
    document.onclick = function(e){
        if(e.target.id !== "u_"+uniqueCategorySelector){
            setIsHidden("hidden");
        }
    };


    return (
        <React.Fragment>
            <form onSubmit={(event)=>{event.preventDefault(); props.subClientSubmitHandler(subClientData())}}
                  className="border rounded-md p-3 border-gray-50 shadow-sm text-sm text-gray-700">
                <div className="flex items-start justify-between">
                    <div className="font-bold text-lg text-gray-600">{t("SubClientInfo")}</div>
                    <div className="flex items-center justify-between">
                        <button onClick={(event)=>{event.preventDefault(); props.showDeleteDialog();}} type="text" className="flex items-center justify-center mr-4 text-add_btn">
                            <img className="w-4 mr-2" src="/icons/remove-circle.svg" alt=""/>
                            {t("RemoveSubClient")}
                        </button>
                        <button type="submit" className="disabled:opacity-70 py-1 flex items-center justify-center mr-4 text-classic_btn" disabled={!formStateSub.isValid || isEmpty}>
                            <img className="w-4 mr-2" src="/icons/save.svg" alt=""/>
                            {t("Save")}
                        </button>
                    </div>
                </div>

                <div className="relative">
                    <div className="text-gray-500 mt-4">{t("SubClientType")}</div>
                    <img onClick={()=>{setIsHidden(`${isHidden === "hidden" ? "" : "hidden"}`)}}
                         className="absolute right-2 top-9 cursor-pointer" src="/icons/arrow-t-down.svg" alt=""/>
                    <input
                        onClick={()=>{setIsHidden(`${isHidden === "hidden" ? "" : "hidden"}`)}}
                        onKeyUp={filterRelation}
                        id={"u_"+uniqueCategorySelector}
                        className="classic-input bg-transparent subclientSelect"
                        type="text"
                        defaultValue={categoryName}
                        onChange={(e) => {setNewCategoryName(e.target.value)}}
                        placeholder={t("SubClientType")}
                    />
                    <ul className={`max-h-56 overflow-y-scroll absolute w-full z-50 bg-white shadow-md border-r border-l rounded-md ${isHidden}`} id={"ul_"+uniqueCategorySelector}>
                        {client && props.categories && props.categories.map((item,key) =>
                            <li onClick={()=>{changeCategory(item)}} className={`p-2 border-b hover:bg-gray-50 cursor-pointer`} key={item._id}>{item.label}</li>
                        )}
                    </ul>
                </div>

                <Input
                    styles="classic-input"
                    marginStyles="mt-4 w-full mr-1"
                    placeholder={t("FirstName")}
                    element="input"
                    id="firstname"
                    type="text"
                    label={t("FirstName")}
                    validators={[VALIDATOR_REQUIRE()]}
                    initialValue={client.firstname || ''}
                    initialValid={!!client.firstname}
                    errorText={t("FieldRequired")}
                    onInput={inputHandlerSub}
                />
                <Input
                    styles="classic-input"
                    marginStyles="mt-4 w-full mr-1"
                    placeholder={t("LastName")}
                    element="input"
                    id="lastname"
                    type="text"
                    label={t("LastName")}
                    validators={[VALIDATOR_REQUIRE()]}
                    initialValue={client.lastname || ''}
                    initialValid={!!client.lastname}
                    errorText={t("FieldRequired")}
                    onInput={inputHandlerSub}
                />
                <Input
                    styles="classic-input"
                    marginStyles="mt-4 w-full mr-1"
                    placeholder="Email"
                    element="input"
                    id="email_address"
                    type="text"
                    label="Email"
                    validators={[VALIDATOR_EMAIL()]}
                    initialValue={client.email || ''}
                    initialValid={!!client.email}
                    errorText={t("ValidEmail")}
                    onInput={inputHandlerSub}
                />
                <Input
                    styles="classic-input"
                    marginStyles="mt-4 w-full mr-1"
                    placeholder={t("PhoneNumber")}
                    element="input"
                    id="phone_number"
                    type="text"
                    label={t("PhoneNumber")}
                    validators={[]}
                    initialValue={client.phone_number || ''}
                    initialValid={true}
                    errorText={t("ValidPhone")}
                    onInput={inputHandlerSub}
                />
                <Input
                    styles="classic-input"
                    marginStyles="mt-4 w-full mr-1"
                    placeholder={t("Location")}
                    element="input"
                    id="location"
                    type="text"
                    label={t("Location")}
                    validators={[]}
                    initialValue={client.location || ''}
                    initialValid={true}
                    errorText={t("FieldRequired")}
                    onInput={inputHandlerSub}
                />
            </form>
        </React.Fragment>
    )
}
export default SubClientForm;