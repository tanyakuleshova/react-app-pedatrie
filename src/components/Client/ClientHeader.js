import AddButton from "../Button/AddButton";
import {t} from "i18next";
import ClientActions from "./ClientActions";

const ClientHeader = (props) => {
    return (
        <div className="mb-4">
            <div className="relative">
                <img className="absolute top-4 left-4 w-3" src="/icons/search.svg" alt=""/>
                <input onKeyUp={(e)=>{props.searchHandler(e.target.value)}} type="text" placeholder="Search" className="pl-10 classic-input"/>
            </div>
            <div className="mt-4 text-sm text-gray-600">
                <div className="mb-2">{t("FilterBy")}</div>
                <ClientActions btn_link={"/clients/new"} btn_text={t("AddClient")}/>
            </div>
        </div>
    )
}
export default ClientHeader;