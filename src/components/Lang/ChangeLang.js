import variables from "../../constants/variables";

const ChangeLang = () => {
    return (
        <div className="flex items-center">
            <img className="mr-2" src="/icons/globe.svg" alt=""/>
            <div className="relative mr-3 w-16">
                <select onChange={(e)=>window.changeLanguage(e.target.value)} name="" id=""
                        className="w-16 text-gray-600 text-md bg-transparent focus:outline-none cursor-pointer">
                    {variables.langs.map(item =>
                        <option key={item.value} value={item.value}>{item.title}
                        </option>
                    )}
                </select>
                <img className="absolute right-2 top-2" src="/icons/arrow-t-down.svg" alt=""/>
            </div>
        </div>
    )
}

export default ChangeLang;