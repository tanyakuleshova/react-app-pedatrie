import React, {Suspense} from 'react';
import {createRoot} from 'react-dom/client';
import App from "./App";
import './styles/index.css';
import './i18n';
createRoot(document.getElementById("root")).render(<Suspense fallback={<div>Loading...</div>}><App /></Suspense>);
