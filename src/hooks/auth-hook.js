import { useState, useCallback, useEffect } from 'react';

export const useAuth = () => {
  const [token, setToken] = useState(false);
  const [refresh_token, setRefreshToken] = useState(false);
  const [userId, setUserId] = useState(false);
  const [userData, setUserData] = useState(false);
  const [verifyData, setVerifyData] = useState(false);

  const login = useCallback((uid, data, token, refresh_token) => {
    setToken(token);
    setRefreshToken(refresh_token);
    setUserId(uid);
    setUserData(data);
    localStorage.setItem(
        'userData',
        JSON.stringify({ userId: uid, userData: data, token: token, refresh_token: refresh_token })
    );
  }, []);

  const logout = useCallback(() => {
    setToken(null);
    setUserId(null);
    setUserData(null);
    setRefreshToken(null);
    localStorage.removeItem('userData');
  }, []);

  const verify = useCallback((data) => {
    setVerifyData(data);
  }, []);

  useEffect(() => {
    const storedData = JSON.parse(localStorage.getItem('userData'));
    if (storedData && storedData.token) {
      login(storedData.userId, storedData.userData, storedData.token, storedData.refresh_token);
    }
  }, [login]);

  return { token, refresh_token, userId, userData, login, logout, verify, verifyData };
};