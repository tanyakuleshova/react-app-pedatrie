import React from "react";
import {useEffect, useState} from "react";
import {clientApi} from "../modules/clients/api/api";
import {useParams} from "react-router-dom";

export const useGetSubClients = () => {
    const userId = useParams().patient_id;
    const [subClients, setSubClients] = useState([]);
    const { getClientRequest } = clientApi();
    useEffect(() => {
        const fetchClient = async () => {
            try {
                const responseData = await getClientRequest(userId);
                const item = responseData.data;
                setSubClients(item.subclients);
            } catch (err) {
                console.log(err);
            }
        };
        fetchClient();

    }, []);

    return {subClients};
}