import { useHttpClient } from "../../../hooks/http-hook";
import {useContext} from "react";
import {AuthContext} from "../../../context/auth-context";
import variables from "../../../constants/variables";


export const clientApi = () => {
    const auth = useContext(AuthContext);
    const { isLoading, sendRequest, error, clearError } = useHttpClient();
    const getClientsRequest = async (page, search="", limit=variables.maxClientPagination) => {
        let url = `/patients?page=${page}&limit=${limit}`;

        if(search){
            url += `&search_string=${search}`
        }
        return await sendRequest(
            url,
            'GET',
            undefined,
            {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + auth.token
            }
        )
    }
    const getClientRequest = async (id, data) => {
        return await sendRequest(
            `/patient?patient_id=${id}`,
            'GET',
            data,
            {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + auth.token
            }
        )
    }
    const addClientRequest = async (data) => {

        const method = JSON.parse(data).patient_id ? 'PATCH' : 'POST';

        return await sendRequest(
            '/patient',
            method,
            data,
            {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + auth.token
            }
        )
    }

    const addSubClientRequest = async (data) => {

        const method = JSON.parse(data).subclient_id ? 'PATCH' : 'POST';

        return await sendRequest(
            '/patient/users/sub',
            method,
            data,
            {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + auth.token
            }
        )
    }

    const deleteClientRequest = async (client) => {

        const url = client.type === "client" ? '/patient?patient_id=' : '/patient/users/sub?user_id=';

        return await sendRequest(
            url + client.id,
            'DELETE',
            undefined,
            {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + auth.token
            }
        )
    }

    const getCategories = async (data) => {
        return await sendRequest(
            `/patient/users/categories`,
            'GET',
            data,
            {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + auth.token
            }
        )
    }

    const addComment = async (data) => {

        return await sendRequest(
            '/patient/comments',
            'POST',
            data,
            {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + auth.token
            }
        )
    }

    const getComments = async (id) => {
        return await sendRequest(
            `/patient/comments?patient_id=${id}`,
            'GET',
            undefined,
            {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + auth.token
            }
        )
    }

    const deleteComment = async (id) => {

        return await sendRequest(
            "/patient/comments?_id="+id,
            'DELETE',
            undefined,
            {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + auth.token
            }
        )
    }

    const editComment = async (data) => {

        return await sendRequest(
            "/patient/comments",
            'PATCH',
            data,
            {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + auth.token
            }
        )
    }

    return { getClientsRequest, getClientRequest, addClientRequest, addSubClientRequest, deleteClientRequest, getCategories, addComment, getComments, deleteComment, editComment, isLoading, error, clearError };
};
