import React, {useEffect, useState} from 'react';
import Layout from "../../layouts/Layout";
import ClientWidget from "../../components/Client/ClientWidget";
import LoadingPageSpinner from "../../components/Loading/LoadingPageSpinner";
import Pagination from "../../components/Pagination/Pagination";
import {usePagination} from "../../hooks/pagination-hook";
import {clientApi} from "./api/api";
import variables from "../../constants/variables";
import ClientsNotFound from "../../components/Client/ClientsNotFound";
import ClientHeader from "../../components/Client/ClientHeader";
import Alert from "../../components/Alert/Alert";


const Clients = () => {

    const { getClientsRequest, isLoading, error } = clientApi();
    const [loadedClients, setLoadedClients] = useState();
    const [search, setSearch] = useState();
    const [updated, setUpdated] = useState(false);
    const {currentPage, setCurrentPage, pageCount, setCountHandler} = usePagination();

    useEffect(() => {
        const fetchClients = async () => {
            try {
                const responseData = await getClientsRequest(currentPage, search);
                const items = responseData.data;
                // if(items && items.length){
                    setLoadedClients(items);
                // }
                setCountHandler(responseData.total, variables.maxClientPagination);
            } catch (err) {
                console.log(err);
            }
        };
        fetchClients();
    }, [currentPage, search, updated]);


    return (
        <Layout>
            {isLoading && (<LoadingPageSpinner/>)}

            <Alert error={error}/>
            {loadedClients && (
                <div className="p-4">
                    <ClientHeader searchHandler={setSearch}/>
                    <Pagination pageCount={pageCount} currentPage={currentPage} setCurrentPage={setCurrentPage}/>
                    <ClientWidget items={loadedClients} setUpdated={setUpdated}/>
                </div>
            )}
            {!isLoading && loadedClients && loadedClients.length === 0 && (
                <div className="text-gray-400 text-center mt-12 text-xl">No Results</div>
            )}
            {!isLoading && !loadedClients && (
                <ClientsNotFound/>
            )}

        </Layout>);
};
export default Clients;