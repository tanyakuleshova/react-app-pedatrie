import React, {useEffect, useState} from 'react';
import Layout from "../../layouts/Layout";
import LoadingPageSpinner from "../../components/Loading/LoadingPageSpinner";
import {clientApi} from "./api/api";
import {useParams} from "react-router-dom";
import ClientCard from "../../components/Client/ClientCard";
import SubClientCard from "../../components/Client/SubClientCard";
import Alert from "../../components/Alert/Alert";
import Tabs from "../../components/Tabs/Tabs";
import ClientComments from "../../components/Client/Comments/ClientComments";
const Clients = () => {

    const userId = useParams().patient_id;
    if(!userId){
        return 404;
    }

    const [loadedClient, setLoadedClient] = useState();
    const { getClientRequest, error, isLoading } = clientApi();

    useEffect(() => {
        const fetchClient = async () => {
            try {
                const responseData = await getClientRequest(userId);
                const item = responseData.data;
                setLoadedClient(item);
            } catch (err) {
                console.log(err);
            }
        };
        fetchClient();

    }, []);


    return (
        <Layout>
            <Tabs active_tab={"info"} patient_id={userId} subclient_id={loadedClient && loadedClient.subclients.length ? loadedClient.subclients[0]._id : ""}/>
            {(isLoading) && (<LoadingPageSpinner />)}

            <Alert error={error}/>

            {loadedClient && (
                <>
                    <ClientCard loadedClient={loadedClient}/>

                    {(loadedClient.subclients && loadedClient.subclients.length > 0) && (
                        <div className="p-4">
                            <div className="font-bold my-4 text-lg">Sub clients</div>

                                <div className="p-2 w-full flex items-start justify-start flex-wrap">
                                    {loadedClient && loadedClient.subclients.map(item =>
                                        <SubClientCard key={item._id} item={item}/>
                                    )}
                                </div>

                            <ClientComments userId = {userId}/>
                        </div>
                    )}
                    {(loadedClient.subclients && !loadedClient.subclients.length) && (
                        <div>
                            <ClientComments userId = {userId}/>
                        </div>

                    )}
                </>
            ) }
        </Layout>);
};
export default Clients;