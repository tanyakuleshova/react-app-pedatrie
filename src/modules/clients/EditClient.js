import React, {useMemo} from "react";
import Layout from "../../layouts/Layout";
import {useForm} from "../../hooks/form-hook";
import AddButton from "../../components/Button/AddButton";
import Alert from "../../components/Alert/Alert";
import {clientApi} from "./api/api";
import {useNavigate, useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import ClientForm from "../../components/Client/ClientForm";
import LoadingPageSpinner from "../../components/Loading/LoadingPageSpinner";
import SubClientForm from "../../components/Client/SubClientForm";
import Modal from "../../components/Dialog/Modal";
import {t} from "i18next";
import Back from "../../components/Button/Back";

const EditClient = () => {

    const {
        addClientRequest,
        getClientRequest,
        addSubClientRequest,
        getCategories,
        deleteClientRequest,
        isLoading,
        error,
        clearError
    } = clientApi();

    const userId = useParams().id;

    const [client, setClient] = useState();
    const [categories, setCategories] = useState();
    const [addSub, setAddSub] = useState(false);
    const [showDialog, setShowDialog] = useState(false);
    const [deleteClientName, setDeleteClientName] = useState();
    const [message, setMessage] = useState(null);
    const [deleteClient, setDeleteClient] = useState();

    const [formState, inputHandler] = useForm();
    const navigate =  useNavigate();

    const fetchClient = async () => {
        if(userId){
            try {
                const responseData = await getClientRequest(userId);

                const clientData = {
                    id: responseData.data._id,
                    firstname: responseData.data.name.firstname,
                    lastname: responseData.data.name.lastname,
                    date_of_birth: responseData.data.info.date_of_birth,
                    description: responseData.data.info.description,
                    subclients:  responseData.data.subclients
                };
                setClient(clientData);

            } catch (err) {
                console.log(err);
            }
        }
    };

    useEffect( () => {
         fetchClient();
    }, [userId]);


    const fetchCategories = async () => {
        try {
            const responseData = await getCategories();
            setCategories(responseData.data.result);
        } catch (err) {
            console.log(err);
        }
    };
    useEffect(() => {
        fetchCategories();
    }, []);

    const clientSubmitHandler = async dob => {

        clearError();
        let data = {
            firstname: formState.inputs.firstname.value,
            lastname: formState.inputs.lastname.value,
            date_of_birth: dob,
            description: formState.inputs.description.value
        };
        if(userId){
            data.patient_id = client.id;
        }

        try {
            const responseData = await addClientRequest(JSON.stringify(data));

            if(!userId){
                setTimeout(()=>{
                    navigate("/clients/edit/"+responseData.data.patient_id);
                })
            }else{
                if(responseData.success){
                    data.subclients = client.subclients;
                    await fetchCategories();
                    await fetchClient();
                    setMessage(t("Success!"));
                    setTimeout(() => {
                        setMessage(null);
                    }, 3000);
                }

            }
        } catch (err) {
            console.log(err);
        }
    }

    const subClientSubmitHandler = async (data) => {

        if(JSON.parse(data).category_id || JSON.parse(data).other_category){
            clearError();
            try {
                const responseData = await addSubClientRequest(data);
                if(responseData.success){
                    setAddSub(false);
                    await fetchCategories();
                    await fetchClient();
                    setMessage("Success!");
                    setTimeout(() => {
                        setMessage(null);
                    }, 3000);
                }
            } catch (err) {
                console.log(err);
            }
        }
    }

    const addSubClient = () => {
        setAddSub({
            name: {
                firstname: "",
                lastname: "",
            },
            contact_info: {
                email_address: "",
                phone_number: ""
            },
            info: {
                location: ""
            },
            relation: {
                category_id: "",
                patient_id: client.id
            }
        });
    }

    const showDeleteDialog = (client) => {
        if(client){
            setShowDialog(true);
            const name = client.name ? (client.name.firstname + " " + client.name.lastname) :
                client.firstname + " " + client.lastname;
            setDeleteClientName(name);
            setDeleteClient({type: client.id ? "client" : "sub", id: client.id || client._id});
        }else{
            setAddSub(false);
        }
    }

    const deleteClientHandler = async () => {
        clearError();
        setShowDialog(false);
        try {
            await deleteClientRequest(deleteClient);
            if(deleteClient.type === "sub"){

                await fetchCategories();
                await fetchClient();
                setMessage(t("Success!"));
                setTimeout(() => {
                    setMessage(null);
                }, 3000);
            }else{
                navigate("/clients");
            }

        } catch (err) {
            setShowDialog(false);
            console.log(err);
        }
    }

    return (
        <Layout>
            <Back/>
            <Modal
                show={showDialog}
                className={"dialog bg-dialog_bg w-width_smx"}
                onCancel={()=>{setShowDialog(false)}}
                headerClass={"dialog-header text-lg text-red-500"}
                header={"Delete client"}
                contentClass="p-8"
                footerClass="p-4"
                footer={
                    <>
                        <div className="w-full cancel-btn mb-2" onClick={()=>{setShowDialog(false)}}>Cancel</div>
                        <div className="w-full delete-btn" onClick={deleteClientHandler}>Delete</div>
                    </>
                }>
                <div className="w-full text-gray-600 text-lg">
                    Are you sure you want to delete the client {deleteClientName}?
                </div>
            </Modal>
            <Alert error={error} message={message}/>
            {isLoading && <LoadingPageSpinner />}
                <>
                    <div className="p-4">
                        {userId ? (client && <ClientForm
                            client={client}
                            clientSubmitHandler={clientSubmitHandler}
                            formState={formState}
                            inputHandler={inputHandler}
                            isLoading={isLoading}
                            showDeleteDialog={()=>{showDeleteDialog(client)}}
                        />) : <ClientForm
                            client={{}}
                            clientSubmitHandler={clientSubmitHandler}
                            formState={formState}
                            inputHandler={inputHandler}
                            isLoading={isLoading}
                            showDeleteDialog={()=>{showDeleteDialog(client)}}
                        />}

                    </div>
                    <div className="flex flex-wrap">
                        {client && categories && client.subclients.map(item =>
                            <div key={item._id} className="w-1/2  my-4 px-4 box-border">
                                <SubClientForm
                                    showDeleteDialog={()=>{showDeleteDialog(item)}}
                                    subclient={item}
                                    subClientSubmitHandler={subClientSubmitHandler}
                                    categories={categories}
                                />
                            </div>
                        )}
                        {addSub && (
                            <div key={addSub.relation.patient_id} className="w-1/2 my-4 px-4 box-border">
                                <SubClientForm
                                    showDeleteDialog={()=>{showDeleteDialog()}}
                                    subclient={addSub}
                                    subClientSubmitHandler={subClientSubmitHandler}
                                    categories={categories}
                                />
                            </div>)}
                        <div className="w-1/2 my-4 px-4 box-border min-h-height_lg">
                            <div className="rounded-md bg-gray-100 h-full shadow-sm flex items-center justify-center">
                                <AddButton disabled={!userId && !addSub} btn_text={t("AddSubClient")} onclick={addSubClient}/>
                            </div>
                        </div>
                    </div>
                </>
        </Layout>
    )
}
export default EditClient;