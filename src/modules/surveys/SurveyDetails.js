import {useParams} from "react-router-dom";
import {surveyApi} from "./api/api";
import React, {useEffect, useState} from "react";
import LoadingPageSpinner from "../../components/Loading/LoadingPageSpinner";
import Layout from "../../layouts/Layout";
import Survey from "../../components/Survey/Survey";
import Back from "../../components/Button/Back";

const SurveyDetails = () => {
    const { getSurveyRequest, isLoading, error, clearError } = surveyApi();
    const [loadedSurvey, setLoadedSurvey] = useState();
    const surveyId = useParams().survey_id;
    if (!surveyId) {
        return 404;
    }
    useEffect(() => {
        const fetchSurvey = async () => {
            try {
                const responseData = await getSurveyRequest(surveyId);
                const item = responseData.data;
                setLoadedSurvey(item);
            } catch (err) {
                console.log(err);
            }
        };
        fetchSurvey();

    }, []);

    return (
        <Layout>
            {(isLoading) && (<LoadingPageSpinner />)}
            <Back/>
            <Survey loadedSurvey={loadedSurvey}/>
        </Layout>
    )
}
export default SurveyDetails;