import React, {useEffect, useState} from "react";
import Layout from "../../../layouts/Layout";
import SurveyWidget from "../../../components/Survey/SurveyWidget";
import {surveyApi} from "../api/api";
import {useNavigate, useParams} from "react-router-dom";
import Alert from "../../../components/Alert/Alert";
import {useGetSubClients} from "../../../hooks/subclients-hook";
import SubClientSelect from "../../../components/SubClients/SubclientSelect";
import LoadingPageSpinner from "../../../components/Loading/LoadingPageSpinner";
import Back from "../../../components/Button/Back";
import {t} from "i18next";
import Comment from "../../../components/Comment/Comment";
import Modal from "../../../components/Dialog/Modal";
import ReminderDialog from "../../../components/Reminder/ReminderDialog";


const AssignSurvey = () => {
    const { getSurveysRequest, assignSurveyRequest, submitReminder, isLoading,  error } = surveyApi();
    const [subClientId, setSubClientId] = useState(useParams().subclient_id);
    const userId = useParams().patient_id;
    const [message, setMessage] = useState("");
    const [errorMessage, setErrorMessage] = useState("");
    const navigate = useNavigate();
    const [showReminderDialog, setShowReminderDialog] = useState(false);
    if(!subClientId || !userId){
        return 404;
    }

    const {subClients} = useGetSubClients();

    const [surveyList, setSurveyList] = useState();
    useEffect(() => {
        const fetchSurveys = async () => {
            try {
                const responseData = await getSurveysRequest(subClientId);
                const items = responseData.data;
                if(items && items.length){
                    setSurveyList(items);
                }
            } catch (err) {
                console.log(err);
            }
        };
        fetchSurveys();
    }, []);

    const assignSurveys = async () => {
        let surveyArray = [];
        const surveys = document.querySelectorAll('.assigned');
        surveys.forEach(item=>{
            surveyArray.push(item.dataset.survey_id);
        });
        if(surveyArray.length){
            try {
                const data = {"subclient_id": subClientId, "survey_ids": surveyArray};
                const responseData = await assignSurveyRequest(JSON.stringify(data));
                if(responseData.success){
                    setMessage("Success!");
                    setTimeout(()=>{
                        setMessage("");
                    }, 2000);
                    // surveys.forEach(item=>{
                    //     item.click();
                    // });
                    navigate(-1);
                }
            } catch (err) {
                console.log(err);
            }
        }else{
            setErrorMessage("Select surveys to assign.");
            setTimeout(()=>{
                setErrorMessage("");
            }, 2000);
        }

    }
    const submitReminderHandler = async (data, method='POST') => {
        const responseData = await submitReminder(JSON.stringify(data), method);
        if (responseData.success) {
            setMessage("Success!");
            setShowReminderDialog(false);
            setTimeout(() => {
                setMessage("");
            }, 2000);
        }else{
            setErrorMessage(responseData.error);
            setTimeout(()=>{
                setErrorMessage("");
            }, 2000);
        }
    }
    return (
       <Layout>
           {isLoading && <LoadingPageSpinner />}
           <Back/>
           <div className="absolute z-50 flex-center w-full">
               <Alert error={errorMessage || error} message={message}/>
           </div>

           <ReminderDialog submitReminderHandler={submitReminderHandler} subClientId={subClientId} show={showReminderDialog} setShow={setShowReminderDialog}/>


           {subClients && subClientId &&
               <div className="flex items-center justify-between m-4 ">
                   <div className="md:w-1/5 relative">
                       <img className="absolute right-2 top-12 z-10" src="/icons/arrow-t-down.svg" alt=""/>
                       <div className="text-sm text-gray-500 mb-1">Choose a sub-client</div>
                       <SubClientSelect subClientId={subClientId} subClients={subClients} setSubClientId={setSubClientId}/>
                   </div>
                   <div className="flex-center">
                       <div onClick={assignSurveys} className="orange-btn mr-4">
                           <div>Send by email</div>
                       </div>
                       <div onClick={()=>{setShowReminderDialog(true)}} className="orange-btn">
                           <div>Generate a link</div>
                       </div>
                   </div>
               </div>}
           {surveyList &&
               <div className="p-4"><SurveyWidget select={true} items={surveyList}/></div>
           }
       </Layout>
    )
}

export default AssignSurvey;