import React, {useEffect, useState} from "react";
import {useParams} from "react-router-dom";
import {surveyApi} from "../api/api";
import Layout from "../../../layouts/Layout";
import LoadingPageSpinner from "../../../components/Loading/LoadingPageSpinner";
import Survey from "../../../components/Survey/Survey";
import Alert from "../../../components/Alert/Alert";
import Back from "../../../components/Button/Back";


const SurveyAnswer = () => {
    const { getSubClientSurvey, getSurveyRequest, submitSurveyRequest, isLoading, error, clearError } = surveyApi();
    const [loadedSurvey, setLoadedSurvey] = useState();
    const surveyId = useParams().survey_id;
    const subclientId = useParams().subclient_id;
    const assign_id = useParams().assign_id;
    const [errorMessage, setErrorMessage] = useState();
    const [updated, setUpdated] = useState(true);
    if (!surveyId || !subclientId || !assign_id) {
        return 404;
    }
    useEffect(() => {
        const fetchSurvey = async () => {
            try {
                const responseData = await getSubClientSurvey(subclientId, surveyId);
                // const responseData = await getSurveyRequest(surveyId);
                const item = responseData.data;
                setLoadedSurvey(item);
            } catch (err) {
                console.log(err);
            }
        };
        fetchSurvey();

    }, [updated]);

    const submitSurvey = async (answers) => {
        try {
            let data = {"subclient_id": subclientId, "survey_id": surveyId, "answers": answers};
            const responseData = await submitSurveyRequest(JSON.stringify(data));
            setUpdated(prevMode => !prevMode);
        } catch (err) {
            console.log(err);
        }
    }
    return (
        <Layout>
            {(isLoading) && (<LoadingPageSpinner />)}
            <Back/>
            <Alert error={errorMessage ? errorMessage : error}/>
            {subclientId && <Survey assign_id={assign_id} setErrorMessage={setErrorMessage}
                                    submitSurvey={submitSurvey} loadedSurvey={loadedSurvey} subclientId={subclientId}/>}
        </Layout>
    )
}

export default SurveyAnswer;