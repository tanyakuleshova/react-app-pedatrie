import React, {useEffect, useState} from "react";
import {Link, useParams} from "react-router-dom";
import Layout from "../../../layouts/Layout";
import Tabs from "../../../components/Tabs/Tabs";
import SurveyWidget from "../../../components/Survey/SurveyWidget";
import {surveyApi} from "../api/api";
import Alert from "../../../components/Alert/Alert";
import {useGetSubClients} from "../../../hooks/subclients-hook";
import LoadingPageSpinner from "../../../components/Loading/LoadingPageSpinner";
import SubClientSelect from "../../../components/SubClients/SubclientSelect";
import {t} from "i18next";
import Comment from "../../../components/Comment/Comment";
import Modal from "../../../components/Dialog/Modal";


const SurveysTab = () => {
    const userId = useParams().patient_id;
    if(!userId){
        return 404;
    }
    const { getSubClientSurveys, deleteSubClientSurvey, isLoading, error } = surveyApi();

    const [loadedSurveys, setLoadedSurveys] = useState([]);
    const [showDeleteDialog, setShowDeleteDialog] = useState(false);
    const [subClientId, setSubClientId] = useState(useParams().subclient_id);
    const [updated, setUpdated] = useState(false);
    const [deleteId, setDeleteId] = useState(false);

    const {subClients} = useGetSubClients();
    useEffect(() => {
        const fetchSurveys = async () => {
            try {
                const responseData = await getSubClientSurveys(subClientId);
                const items = responseData.data;
                setLoadedSurveys(items);
            } catch (err) {
                console.log(err);
            }
        };

        if(subClientId){
            fetchSurveys();
        }

    }, [subClientId, updated])

    const deleteSurveyHandler = async event => {

        event.preventDefault();

        try {
            setShowDeleteDialog(false);
            const responseData = await deleteSubClientSurvey(deleteId);
            setUpdated(prevMode => !prevMode);

        } catch (err) {
        }
    }

    const deleteButtonHandler = (id)=>{
        setDeleteId(id);
        setShowDeleteDialog(true);
    }
    return (
       <Layout>
           <Modal
               show={showDeleteDialog}
               className={"dialog bg-dialog_bg w-width_smx"}
               onCancel={()=>{setShowDeleteDialog(false)}}
               headerClass={"dialog-header text-lg text-red-500"}
               header={t("SurveyDeleting")}
               contentClass="p-4"
               footer={
                   <div className="p-4">
                       <div className="w-full cancel-btn mb-2" onClick={()=>{setShowDeleteDialog(false)}}>{t("Cancel")}</div>
                       <div className="w-full delete-btn" onClick={deleteSurveyHandler}>{t("Delete")}</div>
                   </div>
               }
           >
               <div className="w-full text-gray-600 text-lg my-6">
                   {t("DeleteSurvey?")}
               </div>
           </Modal>

           {isLoading && <LoadingPageSpinner />}
           <Tabs patient_id={userId} subclient_id={subClientId} active_tab={"surveys"}/>
           <Alert error={error}/>
           {subClients && subClientId &&

              <div className="flex items-center justify-between m-4 ">
                  <div className="md:w-1/5 relative">
                      <img className="absolute right-2 top-12 z-10" src="/icons/arrow-t-down.svg" alt=""/>
                      <div className="text-sm text-gray-500 mb-1">Choose a sub-client</div>
                      <SubClientSelect subClientId={subClientId} subClients={subClients} setSubClientId={setSubClientId}/>
                  </div>
                  <Link to={`/clients/surveys-tab-assign/${userId}/${subClientId}`}>
                  <div className="orange-btn">
                      <img className="mr-2" src="/icons/survey.svg" alt=""/>
                      <div>{t("AssignSurveys")}</div>
                  </div>
                  </Link>
              </div>

           }
           {loadedSurveys && (
               <div className="p-4">
                   <SurveyWidget deleteButtonHandler={deleteButtonHandler} border={true} items={loadedSurveys}/>
               </div>
           )}
           {!subClients.length && !isLoading ?
               <div className="p-4">
                   <div className="flex-center text-gray-400 text-lg">
                       <div>
                           {t("HasNoSunClients")}
                           <Link to={`/clients/edit/${userId}`}>
                               <div className="mt-4 orange-btn">{t("AddSubClient")}</div>
                           </Link>
                       </div>
                   </div>
               </div> : !loadedSurveys.length ?
               <div className="p-4">
                   <div className="flex-center text-gray-400 text-lg">{t("HasNoSurveys")}</div>
               </div> : <div></div>
           }


       </Layout>
    )
}

export default SurveysTab;