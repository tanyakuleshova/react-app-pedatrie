import React, {useEffect, useState} from 'react';
import Layout from "../../layouts/Layout";
import LoadingPageSpinner from "../../components/Loading/LoadingPageSpinner";
import PageHeader from "../../components/PageHeader/PageHeader";
import {surveyApi} from "./api/api";
import SurveyActions from "../../components/Survey/SurveyActions";
import SurveyWidget from "../../components/Survey/SurveyWidget";
import Alert from "../../components/Alert/Alert";

const Surveys = () => {
    const { getSurveysRequest, isLoading,  error } = surveyApi();
    const [loadedSurveys, setLoadedSurveys] = useState();
    useEffect(() => {
        const fetchSurveys = async () => {
            try {
                const responseData = await getSurveysRequest();
                const items = responseData.data;
                if(items && items.length){
                    setLoadedSurveys(items);
                }
            } catch (err) {
                console.log(err);
            }
        };
        fetchSurveys();
    }, []);
    return (
        <Layout>
            <Alert error={error}/>
            {isLoading && (<LoadingPageSpinner/>)}
            {loadedSurveys && (
            <div className="p-4">
            <PageHeader>
                <SurveyActions/>
            </PageHeader>
                <SurveyWidget items={loadedSurveys}/>
            </div>)}
        </Layout>);
};
export default Surveys;