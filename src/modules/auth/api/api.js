import { useHttpClient } from "../../../hooks/http-hook";

export const authApi = () => {
    const { isLoading, sendRequest, error, clearError } = useHttpClient();
    const loginRequest = async (data) => {
        return await sendRequest(
            '/auth/admin/login',
            'POST',
            data,
            {
                'Content-Type': 'application/json'
            }
        )
    }
    const verificationRequest = async (data) => {
        return await sendRequest(
            '/auth/admin/verify',
            'POST',
            data,
            {
                'Content-Type': 'application/json'
            }
        )
    }
    return { loginRequest, verificationRequest, isLoading, error, clearError };
};
