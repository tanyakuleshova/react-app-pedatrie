import React, {useContext, useState} from 'react';
import { VALIDATOR_EMAIL, VALIDATOR_MINLENGTH, VALIDATOR_REQUIRE} from '../../utils/validation/validators';
import { AuthContext } from "../../context/auth-context";
import { t } from "i18next";
import { useNavigate } from "react-router-dom";
import { useForm } from "../../hooks/form-hook";
import { authApi } from "./api/api";
import Input from '../../components/Input/Input';
import Alert from "../../components/Alert/Alert";
import LoadingSpinner from "../../components/Loading/LoadingSpinner";
import PublicLayout from "../../layouts/PublicLayout";

const Auth = () => {
    const [isEmailMode, setIsEmailMode] = useState(true);
    const [formState, inputHandler, setFormData] = useForm(
        {
            email: {
                value: '',
                isValid: false
            },
            password: {
                value: '',
                isValid: false
            }
        },
        false
    );
    const { loginRequest, isLoading, error, clearError } = authApi();
    const auth = useContext(AuthContext);
    const navigate =  useNavigate();

    const switchModeHandler = () => {
        clearError();
        if (!isEmailMode) {
            setFormData(
                {
                    email: {
                        value: '',
                        isValid: false
                    },
                    password: {
                        value: '',
                        isValid: false
                    }
                },
                false
            );
        } else {
            setFormData(
                {
                    phone_number: {
                        value: '',
                        isValid: false
                    }
                },
                false
            );
        }
        setIsEmailMode(prevMode => !prevMode);
    };


    const authSubmitHandler = async event => {
        event.preventDefault();
        clearError();
        let data = isEmailMode ? {
            username: formState.inputs.email.value,
            password: formState.inputs.password.value
        } : {
            username: formState.inputs.phone_number.value
        };
        data = JSON.stringify(data);
        const url = isEmailMode ? '/verification/email' : '/verification/phone';
        try {
            const responseData = await loginRequest(data);
            auth.verify({userId: responseData.data.user_id, sendData: data, redirect: url});
            navigate(url);
        } catch (err) {
            console.log(err);
        }
    };
    return (
        <PublicLayout>
            <Alert style={"h-24"} error={error} />
            <div className="flex font-classic">
            <div className="classic-card relative">
                <p className="text-sm font-bold">{t("WelcomeToAccount")}</p>
                <form onSubmit={authSubmitHandler}>
                    {isEmailMode && (
                        <React.Fragment>
                            <Input
                                styles="classic-input"
                                marginStyles="my-4"
                                placeholder={t("EnterEmail")}
                                element="input"
                                id="email"
                                type="email"
                                label={t("Email")}
                                validators={[VALIDATOR_EMAIL()]}
                                errorText={t("ValidEmail")}
                                onInput={inputHandler}
                            />
                            <Input
                                styles="classic-input"
                                marginStyles="my-4"
                                placeholder={t("EnterYourPassword")}
                                element="input"
                                id="password"
                                type="password"
                                label={t("Password")}
                                validators={[VALIDATOR_REQUIRE()]}
                                errorText={t("EnterPassword")}
                                onInput={inputHandler}
                            />
                        </React.Fragment>
                    )}
                    <div onClick={switchModeHandler} className="cursor-pointer hover:text-gray-600 font-classic my-2 text-md text-gray-500">
                        {isEmailMode ? t("SignInWithPhone") : t("SignInWithEmail")}
                    </div>
                    {!isEmailMode && (<Input
                        styles="classic-input"
                        marginStyles="my-4"
                        placeholder="000 000 000"
                        element="input"
                        id="phone_number"
                        type="text"
                        label="Phone number"
                        validators={[VALIDATOR_MINLENGTH(9)]}
                        errorText={t("ValidPhone")}
                        onInput={inputHandler}
                    />)}
                    <button className="w-full classic-btn" type="submit" disabled={!formState.isValid}>
                        <div className="relative">
                            <LoadingSpinner isLoading={isLoading}/>
                            <span>{t("SignIn")}</span>
                        </div>
                    </button>
                </form>
            </div>
            </div>
        </PublicLayout>
    );
};

export default Auth;
