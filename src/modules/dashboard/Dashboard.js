import React, {useContext, useEffect, useState} from 'react';
import { AuthContext } from "../../context/auth-context";
import {t} from "i18next";
import Layout from "../../layouts/Layout";
import {clientApi} from "../clients/api/api";
import ClientWidget from "../../components/Client/ClientWidget";
import LoadingPageSpinner from "../../components/Loading/LoadingPageSpinner";
import SurveyWidget from "../../components/Survey/SurveyWidget";
import {surveyApi} from "../surveys/api/api";
import EmptyClientWidget from "../../components/Client/EmptyClientWidget";
import EmptySurveyWidget from "../../components/Survey/EmptySurveyWidget";
import {documentsApi} from "../documents/api/api";
import DocumentWidget from "../../components/Documents/DocumentWidget";
import EmptyDocumentWidget from "../../components/Documents/EmptyDocumentWidget";

const Dashboard = () => {

    const auth = useContext(AuthContext);
    let name = "Guest.";
    if(auth.userData && auth.userData.name){
        name = auth.userData.name.firstname + " " +auth.userData.name.lastname;
    }
    const [updated, setUpdated] = useState(false);
    const { getClientsRequest, isLoading, error } = clientApi();
    const [loadedClients, setLoadedClients] = useState();


    useEffect(() => {
        const fetchClients = async () => {
            try {
                const responseData = await getClientsRequest(1, "", 2);
                const items = responseData.data;
                if(items && items.length){
                    setLoadedClients(items);
                }

            } catch (err) {
                console.log(err);
            }
        };
        fetchClients();
    }, [updated]);

    const { getSurveysRequest } = surveyApi();
    const [loadedSurveys, setLoadedSurveys] = useState();
    useEffect(() => {
        const fetchSurveys = async () => {
            try {
                const responseData = await getSurveysRequest();
                let items = responseData.data;
                if(items && items.length){
                    items = items.splice(0, 4);
                    setLoadedSurveys(items);
                }
            } catch (err) {
                console.log(err);
            }
        };
        fetchSurveys();
    }, []);

    const { getDocumentsRequest, downloadDocumentRequest } = documentsApi();
    const [loadedDocuments, setLoadedDocuments] = useState();
    useEffect(() => {
        const fetchDocuments = async () => {
            try {
                const responseData = await getDocumentsRequest();
                let items = responseData.data;
                if(items && items.length){
                    items = items.splice(0, 4);
                    setLoadedDocuments(items);
                }
            } catch (err) {
                console.log(err);
            }
        };
        fetchDocuments();
    }, []);

    const downloadHandler = async (_id) => {

        try{
            const response = await downloadDocumentRequest(_id);
            const url = response.data;
            window.open(
                url,
                '_blank'
            );
        }catch (e){}

    }
    return (
        <Layout>
            {isLoading && (<LoadingPageSpinner/>)}
            <div className="p-5">
                <div className="flex-between">
                    <div className="font-bold text-xl flex-center">
                        <img className="mr-2" src="/icons/clients-b.svg" alt=""/>
                        Clients
                    </div>
                    <div className="flex-center font-bold">
                        <a href={"/clients/new"} className="flex-center mr-4">
                            <img className="mr-1 w-3" src="/icons/add-orange.svg" alt=""/>
                            <div className="text-add_btn text-sm cursor-pointer">Add a Client</div>
                        </a>
                        <a href={"/clients"} className="text-blue-400 text-sm cursor-pointer">View All</a>
                    </div>
                </div>

                {loadedClients && (
                    <ClientWidget items={loadedClients} setUpdated={setUpdated}/>
                )}

                {!loadedClients && <div>
                    <EmptyClientWidget/>
                    <EmptyClientWidget/>
                </div>}

                <div className="flex-between mt-6 mb-4">
                    <div className="font-bold text-xl flex-center">
                        <img className="mr-2" src="/icons/clients-b.svg" alt=""/>
                        Surveys
                    </div>
                    <a href={"/surveys"} className="font-bold text-blue-400 text-sm cursor-pointer">View All</a>
                </div>

                {loadedSurveys && (
                    <SurveyWidget items={loadedSurveys}/>
                )}
                {!loadedSurveys && (
                    <div>
                        <EmptySurveyWidget/>
                        <EmptySurveyWidget/>
                        <EmptySurveyWidget/>
                        <EmptySurveyWidget/>
                    </div>
                )}

                <div className="flex items-start flex-between mt-6">
                    <div className="w-1/2">
                        <div>
                            <div className="flex-between mt-6 mb-4">
                                <div className="font-bold text-xl flex-center">
                                    <img className="mr-2" src="/icons/docs.svg" alt=""/>
                                    Documents
                                </div>
                                <a href={"/documents"} className="font-bold text-blue-400 text-sm cursor-pointer">View All</a>
                            </div>
                            {loadedDocuments && (
                                <DocumentWidget downloadHandler={downloadHandler} items={loadedDocuments}/>
                            )}
                            {!loadedDocuments && (
                            <div>
                                <EmptyDocumentWidget/>
                                <EmptyDocumentWidget/>
                                <EmptyDocumentWidget/>
                            </div>
                            )}
                        </div>
                    </div>
                    <div className="w-1/2"></div>
                </div>
            </div>
        </Layout>);
};
export default Dashboard;
