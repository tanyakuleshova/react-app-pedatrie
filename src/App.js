import React from 'react';
import {BrowserRouter as Router, Navigate, Route, Routes} from "react-router-dom";
import Auth from "./modules/auth/Auth";
import Verification from "./modules/auth/Verification";
import Dashboard from "./modules/dashboard/Dashboard";
import { AuthContext } from './context/auth-context';
import {useAuth} from "./hooks/auth-hook";
import Clients from "./modules/clients/Clients";
import Surveys from "./modules/surveys/Surveys";
import Documents from "./modules/documents/Documents";
import {useTranslation} from "react-i18next";
import EditClient from "./modules/clients/EditClient";
import ClientDetails from "./modules/clients/ClientDetails";
import NotFound from "./components/NotFound/NotFound";
import SurveyDetails from "./modules/surveys/SurveyDetails";
import SurveysTab from "./modules/surveys/subclient-survey/SurveysTab";
import SurveyAnswer from "./modules/surveys/subclient-survey/SurveyAnswer";
import AssignSurvey from "./modules/surveys/subclient-survey/AssignSurvey";
import DocumentsTab from "./modules/documents/DocumentsTab";
import DocumentsAdminTab from "./modules/documents/DocumentsAdminTab";
import AssignDocument from "./modules/documents/AssignDocument";
const App = () => {
    const { i18n } = useTranslation();
    window.changeLanguage = (language) => {
        i18n.changeLanguage(language);
    }
    const { token, refresh_token, userId, userData, login, logout, verify, verifyData } = useAuth();
    let routes;
    if(token){
        routes = (
            <Router>
                <Routes>
                    <Route path="/" element={<Navigate replace to="/dashboard" />}/>
                    <Route path="/dashboard" element={<Dashboard />}></Route>
                    <Route path="/clients" element={<Clients />}></Route>
                    <Route path="/clients/new" element={<EditClient />}></Route>
                    <Route path="/clients/details-tab/:patient_id" element={<ClientDetails />}></Route>
                    <Route path="/clients/edit/:id" element={<EditClient />}></Route>
                    <Route path="/clients/surveys-tab/:patient_id/:subclient_id?" element={<SurveysTab />}></Route>
                    <Route path="/clients/surveys-tab-answer/:subclient_id/:survey_id/:assign_id" element={<SurveyAnswer />}></Route>
                    <Route path="/clients/surveys-tab-assign/:patient_id/:subclient_id" element={<AssignSurvey />}></Route>
                    <Route path="/clients/documents-tab-assign/:patient_id/:subclient_id" element={<AssignDocument />}></Route>
                    <Route path="/clients/documents-tab/:patient_id/:subclient_id?" element={<DocumentsTab />}></Route>
                    <Route path="/clients/documents-tab/admin/:patient_id/:subclient_id?" element={<DocumentsAdminTab />}></Route>

                    <Route path="/surveys" element={<Surveys />}></Route>
                    <Route path="/surveys/details/:survey_id" element={<SurveyDetails />}></Route>
                    <Route path="/documents" element={<Documents />}></Route>
                    <Route path="/login" element={<Navigate replace to="/dashboard"/>}></Route>
                </Routes>
            </Router>
        );
    }else{
        routes = (
            <Router>
                <Routes>
                    <Route path="/verification/:item" element={<Verification/>}></Route>
                    <Route path="/" element={<Navigate replace to="/login" />}/>
                    <Route path="/dashboard" element={<Navigate replace to="/login" />}></Route>
                    <Route path="/login" element={<Auth/>}></Route>
                    <Route path='*' element={<NotFound />}/>
                </Routes>
            </Router>
        );
    }

    return (
        <AuthContext.Provider
            value={{
                isLoggedIn: !!token,
                token: token,
                refresh_token: refresh_token,
                userId: userId,
                userData: userData,
                verifyData: verifyData,
                login: login,
                logout: logout,
                verify: verify
            }}
        >
            <main>{routes}</main>
        </AuthContext.Provider>
    );
};

export default App;
